<?php
include_once("../../../../vendor/autoload.php");
use App\BITM\Seip136786\Mobile\Mobile;

$obj = new Mobile();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $obj->prepare($_POST);
    $obj->store();
} else {
    $_SESSION['Err_Msg'] = "Opps ! You're not authoriezed to access this pages";
    header('location:errors.php');
}
