<?php

error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_get('Europe/London');
if(PHP_SAPI == 'cli')
    die('The example should only be run from a web browser');
require_once '../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once '../../../../vendor/autoload.php';
use App\BITM\Seip136786\Mobile\Mobile;
$obj=new Mobile();
$allData=$obj->index();
$objPHPExcel=new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("OFFICE 2007 XLSX Test Document")
        ->setSubject("OFFICE 2007 XLSX Test Document")
        ->setDescription("Test Document for office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");
$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1','SL')
        ->setCellValue('B1','Mobile')
        ->setCellValue('C1','Laptop');
$counter=2;
$serial=0;
foreach($allData as $data) {
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$counter, $serial)
            ->setCellValue('B'.$counter, $data['title'])
             ->setCellValue('C'.$counter, $data['laptop']);
    $counter++;    
}
$objPHPExcel->getActiveSheet()->setTitle('Mobile_list');
$objPHPExcel->setActiveSheetIndex(0);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').'GMT');
header('Cache-Control: cache, must-revalidate');
header('pragma: public');
$objWriter=  PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;